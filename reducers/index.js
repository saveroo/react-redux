	var store = Redux.createStore(counter);

		//render
		function render() {

			var valueEl = document.getElementById('value');
			var value2 = document.getElementById('value2');

			var state = store.getState();
			valueEl.innerHTML = state.count;
			value2.innerHTML = state.sum; 

		};
		store.subscribe(render);

		render();

		//counterreducer
		function counter(currentState, action)
		{
			var DEFAULT_STATE = {count:0, sum:3};
			var nextState = Object.assign({}, currentState);
			if(currentState === undefined)
			{
				nextState = DEFAULT_STATE;
				return nextState;
			}
			switch(action.type){
				case 'DECREMENT':
					nextState.count = currentState.count - 1;
					return nextState;
				case 'INCREMENT':
					nextState.count = currentState.count + 1;
					return nextState;
				case 'SUM':
					nextState.sum = parseInt(action.a) + parseInt(action.b);
					return nextState;
				default:
					return nextState;
			}
		}