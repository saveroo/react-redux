var increase = function()
	{
		return {type: 'INCREMENT'};
	}

var decrease = function()
	{
		return {type: 'DECREMENT'};
	}

var getSum = function(a, b)
	{
		return {type: 'SUM', a:a, b:b};
	}