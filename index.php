<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Redux tester</title>
	<script src="https://unpkg.com/redux@latest/dist/redux.min.js"></script>
	<script src="actions/index.js"></script>
	<script src="reducers/index.js"></script>
</head>
<body>
	<div>
		<p>
		Clicked: <span id="value">0</span> times
		<button id="decrement">Decrease</button>
		<button id="incrementAsync">Increment async</button>
		</p>
		<p>
			<input id='a' type="text"> 
			+ 
			<input id='b' type="text">
			=
			<span id="value2">0</span>
			<button id='sum'>SUM</button>
		</p>
	</div>
	<script>
	

	</script>
	<script src="index.js"></script>
</body>
</html>