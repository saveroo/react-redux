	
		var store = Redux.createStore(counter);

		//render
		function render() {

			var valueEl = document.getElementById('value');
			var value2 = document.getElementById('value2');

			var state = store.getState();
			valueEl.innerHTML = state.count;
			value2.innerHTML = state.sum; 

		};
		store.subscribe(render);

		render();

		//EVENT action
		document.getElementById('decrement')
		.addEventListener('click', function(){
			store.dispatch(decrease());
 		})

 		document.getElementById('incrementAsync')
 		.addEventListener('click', function()
 		{
 			setTimeout(
 					function increment()
 					{
 						store.dispatch(increase());
 					}
 				, 1000)
 		})

 		document.getElementById('sum')
 		.addEventListener('click', function(){

	 		var a = document.getElementById('a').value;
	 		var b = document.getElementById('b').value;
 			store.dispatch(getSum(a,b));
 		})